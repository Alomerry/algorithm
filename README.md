# ioi solution

[![Build Status](https://ci.alomerry.com/buildStatus/icon?job=algorithm&style=flat)](https://ci.alomerry.com/job/algorithm/)

## install

```shell
cd docs
npm install
bundle install

# npm run build
```

## reference

- chrome plugin https://github.com/4074/leetcode-helper
- jekyll tutorial https://www.bootwiki.com/jekyii/jekyll-posts.html